/*Задача №1*/
var age = 18
function showMessage () {
	if (age >= 18 && age <= 60)
		console.log (" Вы ввели верный возраст");
	else if (age < 18)
		console.log ("Ваш возраст мал");
	else if (age > 60)
		console.log ("Вы стары для этого сайта");
}

/*Задача №2*/
var a = 10;
var b = 2;
var rest = a % b;
function showRest () {
	if (rest === 0)
		alert('Делится ' + (a / b));
	else if (rest != 0) 
		alert('Делится с остатком ' + rest);
}

/*Задача №3*/
function triangle () {
for (var line = "*"; line.length < 6; line = line + "*")
	console.log (line); }

/*Задача №4*/
for (var n=1000, q = 1; n > 50; q++) {
	n = n/2
	console.log ("Количество итераций: " + q);
	console.log (n);
}